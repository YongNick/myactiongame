﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public abstract class CharactorControl : MonoBehaviour
{
    // 플레이어 hp,mp,상태
    private int m_hp;
    private int m_maxHp;
    private int m_mp;
    private int m_maxMp;
    private PlayerState m_state;

    // 플레이어 속도
    private float m_fspeed;
    // 중력을 생각했으나 사용하지 않을것같음
    private float m_Gravity;

    // 지금음 사용x
    private bool m_isDodge;
    private bool m_isAttacking;

    //      [SerializeField]
    // 플레이어모델
    public GameObject m_model;

    // 애니메이터, 플레이어 hp,mp게이지
    private Animator m_animator;
    public Image HpGauge;
    public Image MpGauge;

    // 캐릭터컨트롤러를 사용하려헀으나 지금은 사용x
    private CharacterController m_playerController;

    // 초기화작업
    public abstract void Init();
    // 키입력 관련(이동 외 키입력 관련된 모두)
    public abstract void Move();
    // 회피
    public abstract void Dodge();
    // 공격
    public abstract void Attack();
    // 왼쪽으로 뛸때
    public abstract void RunLeft();
    // 오른쪽으로 뛸때
    public abstract void RunRight();
    // 피해를 입었을때
    public abstract void Dameged();
    // 회피판정으로 마나를 얻을때
    public abstract void GetGauge();

    // 변수들의 get전용 
    public Animator GetAnimator
    {
        get { return m_animator; }
    }
    public int GetMaxMP
    {
        get { return m_maxMp; }
    }
    public int GetMP
    {
        get { return m_mp; }
    }
    public int GetMaxHP
    {
        get { return m_maxHp; }
    }
    public int GetHP
    {
        get { return m_hp; }
    }
    public PlayerState GetState
    {
        get { return m_state; }
    }

    public float GetSpeed
    {
        get { return m_fspeed; }
    }
    public float GetGravity
    {
        get { return m_Gravity; }
    }
    public bool GetDodgeState
    {
        get { return m_isDodge; }
    }
    public bool GetAttackingState
    {
        get { return m_isAttacking; }
    }

    //변수들의 set전용
    public void SetHP(int val)
    {
        m_hp = val;
    }
    public void SetMaxHP(int val)
    {
        m_maxHp = val;
    }
    public void SetMP(int val)
    {
        m_mp = val;
    }
    public void SetMaxMP(int val)
    {
        m_maxMp = val;
    }
    public void SetState(PlayerState val)
    {
        m_state = val;
    }
    public void SetSpeed(float val)
    {
        m_fspeed = val;
    }
    public void SetGravity(float val)
    {
        m_Gravity = val;
    }
    public void SetDodge(bool val)
    {
        m_isDodge = val;
    }
    public void SetAttacking(bool val)
    {
        m_isAttacking = val;
    }
    public void SetAnimator(Animator val)
    {
        m_animator = val;
    }

    public void SetAnimatorStateRun(bool val)
    {
        m_animator.SetBool("Run", val);
    }
    public void SetAnimatorStateAttack(bool val)
    {
        m_animator.SetBool("Attack", val);
    }
    public void SetAnimatorStateDead(bool val)
    {
        m_animator.SetBool("Dead", val);
    }
    public void SetAnimatorStateDodge(bool val)
    {
        m_animator.SetBool("Dodge", val);
    }
}