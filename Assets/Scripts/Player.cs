﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public class Player : CharactorControl
{
    public int HP;
    public int MP;
    public int MaxMP;
    public float Radius;
    public float Speed;


    private float m_radius;
    [SerializeField]
    private Transform RadiusCenter;
    public float GetRadius
    {
        get { return m_radius; }
    }
    public void SetRadius(float val)
    {
        m_radius = val;
    }

    public override void Init()
    {
        SetMaxHP(5);
        SetMaxMP(3);
        SetMP(0);
        SetRadius(19);
        SetHP(5);
        SetSpeed(5);
        SetGravity(9.81f);
        SetAnimator(GetComponent<Animator>());
        SetState(PlayerState.Idle);
    }
    public override void Move()
    {
        if (GetState == PlayerState.Idle)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Dodge();
            }
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                Attack();
            }
            if (Input.GetKey(KeyCode.A))
            {
                RunLeft();
            }
            else if (Input.GetKey(KeyCode.D))
            {
                RunRight();
            }
        }
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A))
        {
            SetAnimatorStateRun(false);
        }
    }

    // Use this for initialization
    void Awake()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        AngleSetting();
        Move();
    }

    public override void Dameged()
    {

        int nowHP = GetHP;
        --nowHP;
        SetHP(nowHP);
        HpGauge.fillAmount = (float)nowHP / (float)GetMaxHP;
    }
    public override void GetGauge()
    {
        int nowMP = GetMP;
        ++nowMP;
        SetMP(nowMP);
        MpGauge.fillAmount = (float)nowMP / (float)GetMaxMP;
    }
    public override void Dodge()
    {
        StartCoroutine("dodgeCorutine");
    }

    public override void RunLeft()
    {
        transform.Translate(new Vector3(GetSpeed, 0, 0) * Time.deltaTime);
        m_model.transform.localEulerAngles = new Vector3(0, 90, 0);
        if (GetState == PlayerState.Idle)
        {
            SetAnimatorStateRun(true);
        }
    }
    public override void RunRight()
    {
        transform.Translate(new Vector3(-GetSpeed, 0, 0) * Time.deltaTime);
        m_model.transform.localEulerAngles = new Vector3(0, 270, 0);
        if (GetState == PlayerState.Idle)
        {
            SetAnimatorStateRun(true);
        }
    }
    public override void Attack()
    {
        if (GetMP == GetMaxMP)
        {

        }
    }
    private float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    private float GetDegree(Vector2 from, Vector2 to)
    {
        return Mathf.Atan2(from.y - to.y, to.x - from.x) * Mathf.Rad2Deg;
    }

    private float GetModelLocalEulerAngleY
    {
        get { return m_model.transform.localEulerAngles.y; }
    }

    private void AngleSetting()
    {

        transform.eulerAngles = new Vector3(0, 90 + GetDegree(new Vector2(RadiusCenter.position.x, RadiusCenter.position.z), new Vector2(transform.position.x, transform.position.z)), 0);
        Vector3 tempt = new Vector3(transform.position.x - RadiusCenter.position.x, 0, transform.position.z - RadiusCenter.position.z);
        Vector3 tempt2 = tempt.normalized * GetRadius;
        transform.position = Vector3.Lerp(transform.position, new Vector3(RadiusCenter.position.x + tempt2.x, transform.position.y, RadiusCenter.position.z + tempt2.z), Time.deltaTime * 2);
    }

    IEnumerator dodgeCorutine()
    {
        SetAnimatorStateDodge(true);
        SetState(PlayerState.Dodge);
        StartCoroutine("dodgeMove");
        yield return new WaitForSeconds(0.6f);
        SetAnimatorStateDodge(false);
        yield return new WaitForSeconds(0.6f);
        SetState(PlayerState.Idle);
    }
    IEnumerator dodgeMove()
    {
        bool leftright;
        if (GetModelLocalEulerAngleY == 90)
        {
            leftright = false;
        }
        else if (GetModelLocalEulerAngleY == 270)
        {
            leftright = true;
        }
        else
        {
            leftright = true;
        }
        while (true)
        {
            yield return 0;

            if (GetState == PlayerState.Dodge)
            {
                if (leftright)
                {
                    RunRight();
                }
                else if (!leftright)
                {
                    RunLeft();
                }
            }
            else
            {
                break;
            }
        }
        yield return 0;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("EnemyAttack"))
        {
            switch (GetState)
            {
                case PlayerState.Dodge:
                    GetGauge();
                    break;
                case PlayerState.Attack:
                    break;
                case PlayerState.Idle:
                    Dameged();
                    break;
            }
            /*
            if (GetState == playerState.Dodge)
            {
                getGauge();
            }
            else if (GetState == playerState.Idle)
            {
                dameged();
            }
            else if (GetState == playerState.Attack)
            {

            }*/
        }
    }
}
