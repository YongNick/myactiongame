﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    private int m_hp;
    private float m_rotateSpeed;

    public virtual void init() 
    {
    }
    public virtual void lookplayer() 
    {
    }


    public int GetHP
    {
        get { return m_hp; }
    }
    public float GetSpeed
    {
        get { return m_rotateSpeed; }
    }



    public void SetHP(int val)
    {
        m_hp = val;
    }
    public void SetRotateSpeed(float val)
    {
        m_rotateSpeed = val;
    }
}
