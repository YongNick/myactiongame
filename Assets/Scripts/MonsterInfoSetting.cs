﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public static class MonsterInfoSetting
{
    public static void init()
    {
        Parse();
    }

    private static void Parse()
    {
        TextAsset data = Resources.Load("monsterdata", typeof(TextAsset)) as TextAsset;
        StringReader sr = new StringReader(data.text);

        string source = sr.ReadLine();
        source = sr.ReadLine();
        string[] values;
        int ListLength = MonsterInfoList.GetLength();
        int nowListAddress = 0;
        while (source != null)
        {
            Debug.Log(MonsterInfoList.GetLength());
            if (nowListAddress > ListLength)
            {
                break;
            }
            values = source.Split(' ');
            if (values.Length == 0)
            {
                sr.Close();
                return;
            }
            for (int i = 0; i < values.Length; i++)
            {
                int tempi;
                float tempf;
                switch(i)
                {
                    case 0:
                        tempi = int.Parse(values[i]);
                        MonsterInfoList.SetMonsterID(nowListAddress, tempi);
                        break;
                    case 1:
                        tempi = int.Parse(values[i]);
                        MonsterInfoList.SetMonsterHP(nowListAddress, tempi);
                        break;
                    case 2:
                        tempf = float.Parse(values[i]);
                        MonsterInfoList.SetMonsterSpeed(nowListAddress, tempf);
                        break;
                    case 3:
                        tempi = int.Parse(values[i]);
                        MonsterInfoList.SetMonsterDamage(nowListAddress, tempi);
                        break;

                }
            }
            Debug.Log(MonsterInfoList.GetMonsterID(nowListAddress) + " " +
                MonsterInfoList.GetMonsterHP(nowListAddress) + " " +
                MonsterInfoList.GetMonsterSpeed(nowListAddress) + " " +
                MonsterInfoList.GetMonsterDamage(nowListAddress));
            source = sr.ReadLine();
            nowListAddress++;

        }

    }


    /*
    // Use this for initialization
    void Start()
    {
        Parse();

    }

    // Update is called once per frame
    void Update()
    {

    }*/
}
