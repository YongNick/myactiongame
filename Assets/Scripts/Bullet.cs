﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float Speed;
    public GameObject mob;

    private float m_speed;
    
    public float GetSpeed
    {
        get { return m_speed; }
    }

    public void SetSpeed(float val)
    {
        m_speed = val;
    }


    private void Awake()
    {
        transform.LookAt(mob.transform);
    }
	// Update is called once per frame
	private void Update () 
    {
        move();
	}

    private void move()
    {
        transform.Translate(new Vector3(0, 0, Speed) * Time.deltaTime);
    }
}
