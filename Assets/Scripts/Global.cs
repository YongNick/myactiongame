﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GloNS
{

    // 플레이어 상태
    public enum PlayerState 
    { 
        Idle,    // 평소상태
        Attack,         // 공격상태
        Dodge,          // 회피상태
        Dead            // 사망상태
    }
    // 몬스터 상태
    public enum MonsterState 
    { 
        Idle ,    // 평소
        Attack,         // 공격
        Dead            // 사망
    }
}

