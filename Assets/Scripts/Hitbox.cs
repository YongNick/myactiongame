﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{

    private MeshRenderer mesh;
    private Color meshcolor;
    private GameObject playerObj;
    [SerializeField]
    private GameObject Explosion; 
    // Use this for initialization
    void Awake()
    {
        playerObj = GameObject.Find("Player");
        mesh = gameObject.GetComponent<MeshRenderer>();
        meshcolor = mesh.material.color;
        StartCoroutine("ColorChange");
    }


    IEnumerator ColorChange()
    {
        float a = meshcolor.a;
        while (true)
        {
            yield return new WaitForSeconds(0.05f);
            if (a < 0.7)
            {
                a += 0.02f;
                mesh.material.color = new Vector4(meshcolor.r, meshcolor.g, meshcolor.b, a);
            }
            else
            {
                StartCoroutine("Warning");
                meshcolor.a = a;
                break;
            }
        }
    }
    IEnumerator Warning()
    {
        int temp = 0;
        float a = meshcolor.a;
        bool warningSwitch = true;
        while (true)
        {
            if (temp < 5)
            {
                if (warningSwitch)
                {
                    mesh.material.color = new Vector4(meshcolor.r, meshcolor.g, meshcolor.b, meshcolor.a);
                    warningSwitch = false;
                    temp++;
                }
                else
                {
                    mesh.material.color = new Vector4(meshcolor.r, meshcolor.g, meshcolor.b, 0.3f);
                    warningSwitch = true;
                    temp++;
                }
            }
            else
            {
                GetComponent<Collider>().enabled = true;
                yield return new WaitForSeconds(0.1f);
                Instantiate(Explosion,transform.position,transform.rotation);
                Destroy(this.gameObject);
            }

            yield return new WaitForSeconds(0.15f);
        }
    }

}
