﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class MonsterInfoList
{
    private static int[] m_id;
    private static int[] m_hp;
    private static float[] m_fSpeed;
    private static int[] m_attackDamage;
    private static int m_length;

    public static int GetMonsterID(int val)
    {
        return m_id[val];
    }
    public static int GetMonsterHP(int val)
    {
        return m_hp[val];
    }
    public static float GetMonsterSpeed(int val)
    {
        return m_fSpeed[val];
    }
    public static int GetMonsterDamage(int val)
    {
        return m_attackDamage[val];
    }
    public static int GetLength()
    {
        return m_length;
    }


    public static void SetMonsterID(int address, int val)
    {
        m_id[address] = val;
    }
    public static void SetMonsterHP(int address, int val)
    {
        m_hp[address] = val;
    }
    public static void SetMonsterSpeed(int address, float val)
    {
        m_fSpeed[address] = val;
    }
    public static void SetMonsterDamage(int address, int val)
    {
        m_attackDamage[address] = val;
    }

    public static void Init(int val)
    {
        m_id = new int[val];
        m_hp = new int[val];
        m_fSpeed = new float[val];
        m_attackDamage = new int[val];
        m_length = val;
        Debug.Log(val + " It's");
    }
}